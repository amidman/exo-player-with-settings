package com.example.playerwithsettings

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.sharp.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.daasuu.epf.EPlayerView
import com.daasuu.epf.filter.GlBoxBlurFilter
import com.daasuu.epf.filter.GlBrightnessFilter
import com.daasuu.epf.filter.GlContrastFilter
import com.daasuu.epf.filter.GlFilter
import com.daasuu.epf.filter.GlFilterGroup
import com.example.playerwithsettings.ui.theme.PlayerWithSettingsTheme
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.RepeatModeUtil
import com.google.android.exoplayer2.util.Util


private const val VIDEO_URL = "https://api.cfif31.ru/storeApp/api/Product/2/Video.mp4"


class AmidFilter(
    red: List<Float>,
    green: List<Float>,
    blue: List<Float>,
) :
    GlFilter(DEFAULT_VERTEX_SHADER, fragmentShader(red = red, green = green, blue = blue)) {
    companion object {
        fun fragmentShader(red: List<Float>, green: List<Float>, blue: List<Float>): String =
            "precision mediump float;" +
                    "varying vec2 vTextureCoord;" +
                    "uniform lowp sampler2D sTexture;" +
                    "const highp vec3 weight = vec3(0.2125, 0.7154, 0.0721);" +
                    "void main() {" +
                    "   vec4 FragColor = texture2D(sTexture, vTextureCoord);\n" +
                    "   gl_FragColor.r = dot(FragColor.rgb, vec3(${red[0]}, ${red[1]}, ${red[2]}));\n" +
                    "   gl_FragColor.g = dot(FragColor.rgb, vec3(${green[0]}, ${green[1]}, ${green[2]}));\n" +
                    "   gl_FragColor.b = dot(FragColor.rgb, vec3(${blue[0]}, ${blue[1]}, ${blue[2]}));\n" +
                    "}"
    }
}


class MainActivity : ComponentActivity() {
    private lateinit var ePlayerView: EPlayerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PlayerWithSettingsTheme {

                // A surface container using the 'background' color from the theme

                val red = listOf(
                    0.888F, 0.153F, 0.232F
                )
                val blue = listOf(
                    0.777F, 0.888F, 0.999F
                )
                val green = listOf(
                    0.111F, 0.453F, 0.888F
                )

                val brightness = remember{
                    mutableStateOf(0f)
                }

                val contrast = remember {
                    mutableStateOf(1.2f)
                }

                val blurSize = remember {
                    mutableStateOf(1f)
                }


                Column(modifier = Modifier.fillMaxSize()){
                    AndroidView(factory = { ctx ->
                        ePlayerView = playerView(context = applicationContext)
                        val frameLayout = FrameLayout(ctx)
                        frameLayout.addView(ePlayerView)
                        ePlayerView.onResume()
                        frameLayout
                    })
                    Button(onClick = {
                        ePlayerView.setGlFilter(
                            GlFilterGroup(
//                            AmidFilter(
//                                red = red,
//                                blue = blue,
//                                green = green
//                            ),
                                GlBrightnessFilter().apply {
                                    setBrightness(brightness.value)
                                },
                                GlContrastFilter().apply {
                                    setContrast(contrast.value)
                                },
                                GlBoxBlurFilter().apply {
                                    this.blurSize = blurSize.value
                                }
                            )
                        )
                    }) {
                        Text(text = "Tap to change")
                    }
                    Text(text = "Brightness")
                    Row() {
                        IconButton(onClick = { brightness.value+=0.1f }) {
                            Icon(imageVector = Icons.Sharp.Add, contentDescription = null)
                        }
                        Spacer(modifier = Modifier.width(10.dp))
                        IconButton(onClick = { if (brightness.value >= 0.1f) brightness.value-=0.1f }) {
                            Icon(imageVector = Icons.Default.Home, contentDescription = null)
                        }
                        Text(text = brightness.value.toString())
                    }
                    Text(text = "Contrast")
                    Row() {
                        IconButton(onClick = { contrast.value+=0.1f }) {
                            Icon(imageVector = Icons.Sharp.Add, contentDescription = null)
                        }
                        Spacer(modifier = Modifier.width(10.dp))
                        IconButton(onClick = { if (contrast.value >= 0.1f) contrast.value-=0.1f }) {
                            Icon(imageVector = Icons.Default.Home, contentDescription = null)
                        }
                        Text(text = contrast.value.toString())
                    }
                    Text(text = "Blur")
                    Row() {
                        IconButton(onClick = { blurSize.value+=0.1f }) {
                            Icon(imageVector = Icons.Sharp.Add, contentDescription = null)
                        }
                        Spacer(modifier = Modifier.width(10.dp))
                        IconButton(onClick = { if (blurSize.value >= 0.1f) blurSize.value-=0.1f }) {
                            Icon(imageVector = Icons.Default.Home, contentDescription = null)
                        }
                        Text(text = blurSize.value.toString())
                    }
                }




            }
        }
    }
}


fun playerView(context:Context) : EPlayerView{

    val dataSourceFactory: DataSource.Factory =
        DefaultDataSourceFactory(
            context,
            Util.getUserAgent(context, "player with settings")
        )
    val player = SimpleExoPlayer.Builder(context)
        .setMediaSourceFactory(ProgressiveMediaSource.Factory(dataSourceFactory))
        .build().apply {
            addMediaItem(MediaItem.fromUri(VIDEO_URL))
            prepare()
            playWhenReady = true
            repeatMode = Player.REPEAT_MODE_ALL
        }



    val ePlayerView = EPlayerView(context).apply {
        setSimpleExoPlayer(player)
        layoutParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }
    return ePlayerView
}


